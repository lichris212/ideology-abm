# An agent based model for ideology dynamics 
## Chris Li, originally created for CX530 Winter 2020, in adaptation for an undergraduate thesis. 

This repository contains partially developed code and data which serve as a base framework for an idealized 
simulation of ideology dynamics across agents. Ideologies are abstracted into numerical values
and interactions, contained and mediated across agent classes implemented in python. 

Please refer to the __chrislifinalreport.pdf__ file for a detailed overview of the code, project motivations, etc. 
This readme file only serves as a placeholder readme for the front page of the repo. 
