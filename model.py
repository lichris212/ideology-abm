#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 09:08:23 2020

Model implementations: 
    Parameters
    Classes / Objects
    System functions
    
This file is currently setup to run simulations with the major parameters below
(i.e 6 ideologies, 110 agents, etc.), in a single-run fashion. A single run 
is composed of an initialize() call, and an update() call. 

If you want to do multi-runs, uncomment the for loop at the bottom of the file. 
If you want a visual graph of ideology strengths over time, uncomment the pyCX line,
and make sure the pycxsimulator.py file is in the working directory!
Data collection containers are available near the bottom of the file. 

This file is very messy at the moment. I recommend reading the mechanism details 
in the whitepaper before modifying anything. In its current state, a raw python
run of this file won't produce any printed output--I've been using spyder's
variable explorer to debug, and I recommend you do too, since there's too much 
information stored per run to make verbosity feasible. 
@author: chris
"""

import random
import numpy as np
import copy
import math
from scipy.stats import truncnorm
import pycxsimulator
import matplotlib.pyplot as plt
import statistics

""" 
Parameters 

# of ideologies, # of tokens / ideology
size of vectors / agent/institution
# of agents
# of institutions
time ranges 


"""

num_ideologies = 6      # if you adjust the number of ideologies,make sure to
                        # adjust the interaction matrix as well!
vec_size = 6
num_agents = 110
num_institutions = 5
tag_strength = 0.7


"""
Classes 

tokens
agent
institution
interaction class (interaction type data + interaction functions)
"""
class Token:
    
    tag = -1
    strength = 0.0
    def __init__(self, i_tag, i_strength):
        self.tag = i_tag  # number of ideologies parameter dictates range of tag vals
        self.strength = i_strength
    
"""
Institution carries: 
    vars:
        vector which contains commit values of last x agents, used to 
        switch institution interaction reward distribution to skew towards last x
    
        id
        initial commmits (if matrix[i][j]...)
    
    methods:
        function that moderates tag 
"""
class Institution:
    
    
    def __init__(self, id):
        self.id = id
        self.tag = -1
        self.commits = [0.0 for i in range(0,num_ideologies)]
        
    def reset(self, i_id):
        self.tag = -1
        self.id = i_id
        self.commits = [0.0 for i in range(0, num_ideologies)]
        
        
    def tagControl(self):
        highest = 0.000001
        idx = 0
        for value in self.commits:
            if value > highest:
                highest = value
            self.commits[idx] = 0.0
            idx += 1        
        return idx
        
class Agent:
    
    ### Init methods ###
    
    def __init__(self, a_id, i_size):
        global num_agents, num_institutions
        self.id = a_id        
        self.size = i_size
        
        self.beliefs = []
        self.ideas = []
        self.values = []
        self.bivs = [self.beliefs, self.ideas, self.values]
        
        self.commits = []
        for i in range (0, num_ideologies):
            self.commits.append(0)
        self.tag = -1
        
        ### Lists to contain running reward values per agent/institution
        self.a_list = [[0,0] for ag in range(num_agents)]
        self.i_list = [[0,0] for i in range(num_institutions)]
        
        self.temperature = 0.5 # temperature in boltzmann exploration
        
    def reset(self, a_id, i_size):
        self.tag = -1
        self.id = a_id
        self.size = i_size
                
    def generateTokens(self):
        tempList = list(range(0, num_ideologies))
        tempList.remove(self.tag)
        
        # Loops to insert tokens 
        for i in range(0,vec_size):
            randval = random.uniform(0.001, 1.0)  # used for probability in token generation
            if randval < tag_strength:
                t_belief = Token(self.tag, random.random())
                self.beliefs.append(t_belief)
                self.commits[t_belief.tag] += t_belief.strength
            else:
                t_belief = Token(random.choice(tempList), random.random())
                self.beliefs.append(t_belief)
                self.commits[t_belief.tag] += t_belief.strength
    
        for i in range(0,vec_size):
            randval = random.uniform(0.001, 1.0)  # used for probability in token generation
            if randval < tag_strength:
                t_idea = Token(self.tag, random.random())
                self.ideas.append(t_idea)
                self.commits[t_idea.tag] += t_idea.strength
            else:
                t_idea = Token(random.choice(tempList), random.random())
                self.ideas.append(t_idea)
                self.commits[t_idea.tag] += t_idea.strength
     
        for i in range(0,vec_size):
            randval = random.uniform(0.001, 1.0)  # used for probability in token generation
            if randval < tag_strength:
                t_value = Token(self.tag, random.random())
                self.values.append(t_value)
                self.commits[t_value.tag] += t_value.strength
            else:
                t_value = Token(random.choice(tempList), random.random())
                self.values.append(t_value)
                self.commits[t_value.tag] += t_value.strength
                
        """ Reinforcement component. I'll initially be implementing 
        a boltzmann exploration policy function for the agents, using
        a recursive sample-average to maintain history of rewards"""
    def runningAgentAverage(self, reward, id):
        # for some action a, let Qk denote the average of its first k rewards
        nextQk = self.a_list[id][0] + (1/(self.a_list[id][1]+1)) * (reward - self.a_list[id][0])
        self.a_list[id][0] = nextQk
        self.a_list[id][1] += 1
        
    def selectAgentAction(self):
        
        ### Returns the idx of the agent selected to interact with 
        
        summation = 0
        idx = 0
        highest = -1
        highestIdx = -1
        for a in self.a_list:
            summation += math.exp(a[0] / self.temperature)
        for a in self.a_list:
            prob = self.prob_boltzmann(a[0], summation)
            if prob > highest: 
                highest = prob
                highestIdx = idx
            if random.random() < prob:
                return idx
            idx += 1
        return highestIdx
    
    def runningInstAverage(self, reward, id):
        # for some action a, let Qk denote the average of its first k rewards
        nextQk = self.i_list[id][0] + (1/(self.i_list[id][1]+1)) * (reward - self.i_list[id][0])
        self.i_list[id][0] = nextQk
        self.i_list[id][1] += 1
            
    def selectInstAction(self):
        
        ### Returns the idx of the institution selected to interact with 
        
        summation = 0
        idx = 0
        highest = -1
        highestIdx = -1
        for i in self.i_list:
            summation += math.exp(i[0] / self.temperature)
        for i in self.i_list:
            prob = self.prob_boltzmann(i[0], summation)
            if prob > highest: 
                highest = prob
                highestIdx = idx
            if random.random() < prob:
                return idx
            idx += 1
        return highestIdx
    
    
    def prob_boltzmann(self, Qk, listSum):
        prob_action = (math.exp(Qk/self.temperature)) / listSum
        return prob_action
    
class Interact:
        
    """
    action function dictates the results of two interacting agents. 
    the function's modifying output relies on these variables:
        interaction distance (closer distance means exaggerated changes)
        interaction type between two tokens
        strengths of respective tokens
    a value [0,1) is uniformly selected to be the distance
    
    """
    def __init__(self):
            #sample 3x3 interaction matrix
        #   0 : self 
        #   1 : positive
        #   2 : negative
        #self.matrix = [[0, 1, 1],
        #          [1, 0, 1],
        #          [1, 1, 0]]   
        self.matrix = [[0,1,2,1,2,1],
                       [1,0,2,1,2,2],
                       [1,2,0,1,2,1],
                       [1,2,1,0,2,2],
                       [1,2,1,2,0,1],
                       [1,2,1,2,2,0]]
        
        
        ### Returning binary entropy function
        ### this will be used as stddev in the reward distribution function--
        ### it follows the principle of information gain in information theory,
        ### where there might be more information gained with conflicting ideas
    def entropy(self, strength):
        try:
            ent = (-strength * math.log2(strength) - (1-strength)*math.log2(1-strength))
            return ent
        except:
            print("math domain error")
        
    
        ### Picks reward value from a truncated normal distribution with range 
        ### (-1.0, 1.0) with mean determined by interaction type, 
        ### and std dev determined by entropy function above
    def reward(self, std, i_type):
        
        if i_type == 1:
            rGen = get_truncated_normal(mean = 0.5, sd = std, low=-1.0, upp=1.0)
        elif i_type == 2:
            rGen = get_truncated_normal(mean = -0.5, sd = std, low=-1.0, upp=1.0)
        else: 
            rGen = get_truncated_normal(mean = 0, sd = std, low=-0.33, upp=1.0)
        return rGen.rvs()
    
    def strengthChange(self, token, reward):
        change = (abs(np.arcsin(reward)) + 0.5)
        token.strength = token.strength * change
        if token.strength > 1.0:
            token.strength = .99
        return token.strength
         
    def action(self, a1, a2):
        
        for i in range (0, 3):
            tok1 = random.choice(a1.bivs[i])
            tok2 = random.choice(a2.bivs[i])
            a1.commits[tok1.tag] -= tok1.strength
            a2.commits[tok2.tag] -= tok2.strength
            
            if (tok1.strength + tok2.strength)/2 == 0.0:
                b_ent = self.entropy(0.001)
            else:
                b_ent = self.entropy((tok1.strength + tok2.strength)/2)
            r_value1 = self.reward(b_ent, self.matrix[tok1.tag][tok2.tag])
            r_value2 = self.reward(b_ent, self.matrix[tok2.tag][tok1.tag])
            
            a1.commits[tok1.tag] += self.strengthChange(tok1, r_value1)
            a2.commits[tok2.tag] += self.strengthChange(tok2, r_value2)
            a1.runningAgentAverage(r_value1, a2.id)
            a2.runningAgentAverage(r_value2, a1.id)
    
    # Institution action
    def instAction(self, a, inst):
        
        # Binary entropy is set to 0.65 for now to signify that institutions
        # should always have some minimum of influential power
        b_ent = 0.65 
        for i in range (0, 3):
            tok = random.choice(a.bivs[i])
            a.commits[tok.tag] -= tok.strength
            
            r_value = self.reward(b_ent, self.matrix[tok.tag][inst.tag])
            a.commits[tok.tag] += self.strengthChange(tok, r_value)
            a.runningInstAverage(r_value, inst.id)
            
        for i in range (0, num_ideologies):
            inst.commits[i] += a.commits[i]
        
"""
High level system objects and functions

agent vector
tagging function

initialization
updating
data collection

"""
ags = []        #   agents
insts = []      # institutions 

strength_arr = [[],[],[],[],[],[]]
changes_arr = [[],[],[],[],[],[]]
i_strengths = {}
i_changes = {}
i_changesPerStep = {}
adherences = []

time = 1
timesteps = 200
timePerInstAdjust = 5 # after how many timesteps should the agent modify its' tag
        
### Methods ###

def get_truncated_normal(mean, sd, low, upp):
    return truncnorm((low - mean) / sd, (upp - mean) / sd, loc=mean, scale=sd)

def initialQuery():
    global i_strengths, strength_arr, changes_arr
    for i in range (0, num_ideologies):
        strength = 0
        for ag in ags:
            strength += ag.commits[i]
        i_strengths[i] = strength
        i_changes[i] = 0
        
    #strength_arr = [[i_strengths[0]],[i_strengths[1]],[i_strengths[2]]]
    #changes_arr = [[i_changes[0]],[i_changes[1]],[i_changes[2]]]
def queryStrengths():
    global i_strengths, i_nextStrengths, i_changes, i_changesPerStep, timestep
    for i in range (0, num_ideologies):     # finding changes in id strength
        strength = 0
        for ag in ags:
            strength += ag.commits[i]
        i_changes[i] += strength - i_strengths[i]
        i_changesPerStep[i] = i_changes[i] / time
        i_strengths[i] = strength
    
    
def generateAgents():
    global ags
    agCount = 0
    temp = Agent(agCount, vec_size)
    for i in range(0, num_agents):
        temp.reset(agCount, vec_size)
        tempPutter = copy.deepcopy(temp)
        tempPutter.tag = np.random.randint(0, num_ideologies)
        tempPutter.generateTokens()
        ags.append(tempPutter)
        agCount += 1
        
def generateInsts():
    global insts
    instCount = 0
    temp = Institution(instCount)
    for i in range(0, num_institutions):
        temp.reset(instCount)
        tempPutter = copy.deepcopy(temp)
        tempPutter.tag = np.random.randint(0, num_ideologies)
        insts.append(tempPutter)
        instCount += 1
        
def generateTokens(self):
        tempList = list(range(0, num_ideologies))
        tempList.remove(self.tag)
        
        # Loops to insert tokens 
        for i in range(0,vec_size):
            randval = random.uniform(0.001, 1.0)  # used for probability in token generation
            if randval < tag_strength:
                t_belief = Token(self.tag, random.random())
                self.beliefs.append(t_belief)
                self.commits[t_belief.tag] += t_belief.strength
            else:
                t_belief = Token(random.choice(tempList), random.random())
                self.beliefs.append(t_belief)
                self.commits[t_belief.tag] += t_belief.strength
        
def initialize():
    # just driving for now
    global ags, insts, time
    time = 1
    ags.clear()
    insts.clear()
    generateAgents()
    generateInsts()
    initialQuery()
    
def update():
    global ags, time, insts, timePerInstAdjust, strength_arr, change_arr
    interactor = Interact()
    while time < timesteps:
        
        
        # institution block: 9-5, 24/7, except that the agents have "choice"
        if (time % 24) >= 9 and (time % 24) <= 17:
            # institution tag moderator
            if (time % timePerInstAdjust) == 0:
                for inst in insts:
                    inst.tag = ags[inst.tagControl()].tag
            for ag in ags:
                instIdx = ag.selectInstAction()
                interactor.instAction(ag, insts[instIdx])
        # agent-to-agent block
        else: 
            for ag in ags:
                ag2idx = ag.selectAgentAction()
                interactor.action(ag, ags[ag2idx])
                
        queryStrengths()
        for i in range(0,num_ideologies):
            strength_arr[i].append(i_strengths[i])
            changes_arr[i].append(i_changesPerStep[i])
        time += 1
        
def observe():
    global ags, insts, i_strengths

    # Plotting moving graph of i_strengths
    plt.figure(1)
    plt.cla()
    for i in range(0, num_ideologies):
        label1 = f"ideology {i} strength over time"
        plt.plot(strength_arr[i], label = label1)
    plt.legend()
    
    
    plt.figure(2)
    plt.cla()
    for i in range(0, num_ideologies):
        label2 = f"ideology {i} change in strength/timestep"
        plt.plot(changes_arr[i], label = label2)
    plt.legend()    
    
def postQuery():
    global ags, adherences
    
    # number of agents whose top commit is their beginning tag 
    tagAdherence = 0
    for ag in ags:
        max_value = max(ag.commits)
        max_index = ag.commits.index(max_value)
        if max_index == ag.tag:
            tagAdherence += 1
    adherences.append(tagAdherence)
    
#pycxsimulator.GUI().start(func=[initialize, observe, update])
"""
 Just driving for now
"""

#numsims = 20
#for i in range (0, numsims):
initialize()
update()
postQuery()    

    
